# Docker-TF2-Automatic
#### Maintained By: Isaac Towns <Zelec@timeguard.ca>
This repository is for my unofficial [Docker Image](https://hub.docker.com/r/zelec/tf2-automatic) for [tf2-automatic](https://github.com/Nicklason/tf2-automatic) made by [Nicklas Pedersen](https://github.com/Nicklason)

[tf2-automatic](https://github.com/Nicklason/tf2-automatic) is a fully automated TF2 trading bot. It advertizes through [backpack.tf](https://backpack.tf) using prices from [prices.tf](https://prices.tf)

To get this working, you will need a separate Steam Account (For the love of god, don't use your main account) with a mobile authenticator.  
To simplify 2FA setup, it would be best to use [Steam Desktop Authenticator](https://github.com/Jessecar96/SteamDesktopAuthenticator) as a replacement for the Steam Mobile App, since you can easily extract the secret keys used to generate 2FA codes and authenticate mobile trade confirmations and have the bot manage it all for you.

## Quickstart
1. Make a docker-compose.yml file like this
```yaml
version: '2'
services:
  tf2-automatic:
    image: zelec/tf2-automatic:latest
    restart: unless-stopped
    env_file:
      - .env
    volumes:
      - ./config:/config
      - ./logs:/logs
```
2. Copy the template.env file to .env
3. Edit the .env file to your needs
  * If you don't know your PUID & PGID values are, type `echo -n "PUID:" && id -u "$USER" && echo -n "PGID:" && id -g "$USER"` into a terminal
4. Run `docker-compose up`



#!/usr/bin/dumb-init /bin/bash
echo "Docker-TF2-Automatic starting up..."
echo "If you find any issues/bugs, open them at https://gitlab.com/Zelec/docker-tf2-automatic/issues"

# Ripped from Linuxserver.io baseimages.
PUID=${PUID:-911}
PGID=${PGID:-911}

groupmod -o -g "$PGID" user
usermod -o -u "$PUID" user
chown -R user:user /app
chown -R user:user /config
chown -R user:user /defaults
chown -R user:user /logs
find /config -type d -exec chmod 700 {} \;
find /config -type f -exec chmod 600 {} \;
echo "Pre-launch setup done. Starting PM2 with id $PUID:$PGID..."
su -s /bin/bash user -c 'export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin && cd /app && pm2-runtime start ecosystem.json'


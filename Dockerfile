# Docker-TF2-Automatic, by Isaac Towns <Zelec@timeguard.ca>

# Rather than making the git repo a submodule of this repo, it would be better off to change the docker file as needed.
FROM alpine/git:1.0.7 AS repo

# Normally alpine/git uses /git for it's workdir, however it's declared as a volume in the upstream resource
# so it has a hard time keeping data intact inbetween layers.
RUN mkdir /app
WORKDIR /app
RUN git clone https://github.com/Nicklason/tf2-automatic.git . && \
    git checkout `git describe --abbrev=0 --tags --match "v[0-9]*" $(git rev-list --tags --max-count=1)`

# Simple ecosystem.json modified from the master tf2-automatic repo, minus the env variables since Docker will be taking care of that.
FROM library/alpine:3.11.3 AS ecosystem
COPY --from=repo /app/ecosystem.template.json /tmp/ecosystem.template.json
RUN echo "Creating docker-friendly ecosystem.json file..." && \
    apk --no-cache --update-cache add jq && \
    cat /tmp/ecosystem.template.json | jq -r "del(.apps[].env)" > /tmp/ecosystem.json


# Uses NodeJS 10 as the base image, then installs PM2 for process handling
FROM library/node:10-alpine AS docker-tf2-automatic
LABEL maintainer Isaac Towns <Zelec@timeguard.ca>
COPY --from=repo /app/ /app
COPY --from=ecosystem /tmp/ecosystem.json /app/ecosystem.json
WORKDIR /app
# Dependency install
RUN rm -rf ./.git && \
    apk --no-cache --update-cache upgrade && \
    apk --no-cache add shadow bash dumb-init && \
    useradd -u 911 -U -d /tmp -s /bin/false user && \
    usermod -G users user && \
    npm install pm2 -g && \
    npm install && \
    npm cache clean --force && \
    npm run build --if-present && \
    mkdir -p /logs /config /defaults && \
    ln -s /config /app/files && \
    ln -s /logs /app/logs
# Volume declarations
VOLUME ["/logs", "/config"]
COPY entrypoint.sh /
ENTRYPOINT ["/usr/bin/dumb-init"]
CMD ["/entrypoint.sh"]

